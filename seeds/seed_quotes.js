const fs = require('fs');
const path = require('path');

const entireData = JSON.parse(fs.readFileSync(path.join(__dirname, '../data/db.json'), 'utf8'));

const data = entireData.quotes;

exports.seed = function(knex) {
    return knex('quotes').del()
        .then(function () {
            return knex('quotes').insert(data);
        });
};