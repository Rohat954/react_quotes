/**
 * @param { import("knex").Knex } knex
 * @returns {Knex.SchemaBuilder}
 */
exports.up = function(knex) {
    return knex.schema.createTable('quotes', function(table) {
        table.string('id').primary();
        table.string('author');
        table.string('text');
        table.string('priority');
        table.string('verified');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('quotes');
};
