import {IQuote} from "./types/quotes";

const baseUrl = "http://localhost:3001/api";

export const getAllQuotes = async (): Promise<IQuote[]> => {
    const res = await fetch(`${baseUrl}/quotes`, { cache: 'no-store' });

    if (!res.ok) {
        console.log(await res.text());
        throw new Error(`HTTP error! status: ${res.status}`);
    }

    return await res.json();
}

export const addQuote = async (quote: {
    author: string;
    verified: boolean;
    id: string;
    text: string;
    priority: number
}): Promise<IQuote> => {
    const res = await fetch(`${baseUrl}/quote`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(quote)
    });

    if (!res.ok) {
        throw new Error(`HTTP error! status: ${res.status}`);
    }

    return await res.json();
}

export const editQuote = async (quote: {
    author: string;
    verified: boolean;
    id: string;
    text: string;
    priority: string
}): Promise<IQuote> => {
    const res = await fetch(`${baseUrl}/quotes/${quote.id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(quote)
    });
    return await res.json();
}

export const deleteQuote = async (id: string): Promise<void> => {
    await fetch(`${baseUrl}/quotes/${id}`, {
        method: "DELETE"
    });
}