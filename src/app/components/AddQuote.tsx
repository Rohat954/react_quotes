'use client';

import { FaPlus } from "react-icons/fa6";
import React, {FormEventHandler} from "react";
import Modal from "@/app/components/Modal";
import {addQuote} from "../../../api";
import {useRouter} from "next/navigation";
import { v4 as uuidv4 } from 'uuid';

const AddQuote = () => {
    const router = useRouter();
    const [modalOpen, setModalOpen] = React.useState(false);
    const [quote, setQuote] = React.useState("");
    const [author, setAuthor] = React.useState("");
    const [verified, setVerified] = React.useState(false);
    const [error, setError] = React.useState("");

    const handleSubmitNewQuote: FormEventHandler<HTMLFormElement> = async (e) => {
        e.preventDefault();

        if (!author.trim() || !quote.trim()) {
            setError("Both author and quote fields must be filled out.");
            return;
        }

        setError("");

        await addQuote({
            id: uuidv4(),
            author: author,
            text: quote,
            priority: 1,
            verified: verified
        });
        setModalOpen(false);
        setQuote("");
        setAuthor("");
        router.refresh();
    };

    return <div>
        <button onClick={() => setModalOpen(true)} className="btn btn-primary w-full">Add quote
            <FaPlus/>
        </button>

        <Modal modalOpen={modalOpen} setModalOpen={setModalOpen}>
            <form onSubmit={handleSubmitNewQuote}>
                <h3 className="font-bold text-lg">Add new quote</h3>
                <input type="text" placeholder="Author's name"
                       className="input input-bordered input-primary input-ghost w-1/2" value={author}
                       onChange={e => setAuthor(e.target.value)}/>

                <div className="modal-action flex flex-col space-y-4">
            <textarea placeholder="Type quote here"
                      className="input input-bordered input-primary input-ghost w-full h-20" value={quote}
                      onChange={e => setQuote(e.target.value)}/>
                    <div className="form-control flex items-center justify-center">
                        <label className="flex gap-3 label cursor-pointer">
                            <span className="label-text ">Verified</span>
                            <input type="checkbox" checked={verified} className="checkbox checkbox-primary" onChange={() => setVerified(!verified)}/>
                        </label>
                    </div>
                    {error && <p className="text-red-500">{error}</p>}
                    <button type="submit" className="btn btn-primary ">Add</button>
                </div>
            </form>
        </Modal>
    </div>;
};

export default AddQuote;