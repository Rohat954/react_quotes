const express = require('express');
const knex = require('knex');
const config = require('./knexfile.js');
const bodyParser = require('body-parser');
const cors = require('cors');

const db = knex(config.development);
const app = express();

app.use(cors());
app.use(bodyParser.json());

app.get('/api/quotes', async (req, res) => {
    const data = await db.select('*').from('quotes');
    res.json(data);
});

app.post('/api/quote', async (req, res) => {
    const quote = req.body;
    const [id] = await db('quotes').insert(quote);
    res.json({ ...quote, id });
});

app.put('/api/quotes/:id', async (req, res) => {
    const { id } = req.params;
    const quote = req.body;
    await db('quotes').where({ id }).update(quote);
    res.json(quote);
});

app.delete('/api/quotes/:id', async (req, res) => {
    const { id } = req.params;
    await db('quotes').where({ id }).del();
    res.status(204).send();
});

app.listen(3001, () => {
    console.log('Server is running on port 3001');
});